import mapboxgl from "mapbox-gl";
import React, {useEffect, useRef, useState} from "react";
import ReactDOM from "react-dom";
import "./Map.css";
import {Popup} from "./components/popup";

mapboxgl.accessToken =
    "pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA";

const Marker = ({onClick, children, feature}) => {
    const _onClick = () => {
        onClick(feature.properties);
    };

    return (
        <button onClick={_onClick} className="marker">
            {children}
        </button>
    );
};

const Map = ({location, city}) => {

    const [open, setOpen] = useState(false);

    const [localProperties, setLocalProperties] = useState(null)

    const mapContainerRef = useRef(null);

    const updatedData = location.map(function (obj) {
        return {
            "type": "Feature",
            "properties": {
                "name": obj.name,
                "church_address_street_address": obj.church_address_street_address,
                "phone_number": obj.phone_number,
                "url": obj.url

            },
            "geometry": {
                "coordinates": [+obj.longitude, +obj.latitude],
                "type": "Point"
            }
        }
    })


    // Initialize map when component mounts
    useEffect(() => {
        const map = new mapboxgl.Map({
            container: mapContainerRef.current,
            style: "mapbox://styles/mapbox/streets-v11",
            center: [city.lon, city.lat],
            zoom: 10,
        });
        // Render custom marker components
        updatedData.forEach((feature) => {
            // Create a React ref
            const ref = React.createRef();
            // Create a new DOM node and save it to the React ref
            ref.current = document.createElement("div");
            // Render a Marker Component on our new DOM node
            ReactDOM.render(
                <Marker onClick={markerClicked} feature={feature}/>,
                ref.current
            );

            // Create a Mapbox Marker at our new DOM node
            new mapboxgl.Marker(ref.current)
                .setLngLat(feature.geometry.coordinates)
                .addTo(map);
        });

        // Add navigation control (the +/- zoom buttons)
        map.addControl(new mapboxgl.NavigationControl(), "top-right");

        // Clean up on unmount
        return () => map.remove();
    }, [location]);

    const markerClicked = (properties) => {
        setLocalProperties(properties)
        setOpen(true)
    };

    return <div>
        <div className="map-container" ref={mapContainerRef}/>
        {open ? <Popup localProperties={localProperties} closePopup={() => setOpen(false)}/> : null}</div>;
};

export default Map;
