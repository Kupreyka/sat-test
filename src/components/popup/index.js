import React from 'react'
import style from './popup.module.css'

export const Popup = ({localProperties, closePopup}) => {
    return (
        <div className={style.popupContainer}>
            <div className={style.popupBody}>
                <button onClick={closePopup}>Close X</button>
                <div>
                    <h1>{localProperties.name}</h1>
                    <p>{localProperties.church_address_street_address}</p>
                    <p><a href={'tel:' + localProperties.phone_number}>{localProperties.phone_number}</a></p>
                    {localProperties.url &&
                        <p><a href={localProperties.url} target="_blank">{localProperties.url}</a></p>}
                </div>
            </div>
        </div>
    );
};
