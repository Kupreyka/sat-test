import React, {useEffect, useState} from "react";
import Map from "./Map";
import axios from "axios";
import "./App.css"

const city = [
    {name: 'New York', lon: -73.935242, lat: 40.730610},
    {name: 'Chicago', lon: -87.623177, lat: 41.881832},
    {name: 'Boston', lon: -71.057083, lat: 42.361145},
    {name: 'Oakland', lon: -122.271111, lat: 37.804363},
]

function App() {


    const [location, setLocation] = useState([])
    const [localCity, setLocalCity] = useState({name: 'New York', lon: -73.935242, lat: 40.730610})

    useEffect(() => {
        axios.get(`https://apiv4.updateparishdata.org/Churchs/?lat=${localCity.lat}&long=${localCity.lon}&pg=1`)
            .then(response => {
                setLocation(response.data)
            })
    }, [localCity])


    return <div>

        <div className="select">
            <select onChange={event => setLocalCity(JSON.parse(event.target.value))}>
                {city.map((el, index) => <option value={JSON.stringify(el)} key={index}>{el.name}</option>)}
            </select>
        </div>
        <Map location={location} city={localCity}/>;
    </div>
}

export default App;
